// Copyright 2021 Emiliano Vaz Fraga

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 	http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package test

import (
	"bytes"
	"fmt"
	"strings"
)

func IsTrue(actual bool, variableName string) (bool, string) {
	if !actual {
		return false, fmt.Sprintf("IsTrue failed for %s", variableName)
	}

	return true, ""
}

func IsFalse(actual bool, variableName string) (bool, string) {
	if actual {
		return false, fmt.Sprintf("IsFalse failed for %s", variableName)
	}

	return true, ""
}

func StringEquals(actual string, expected string, variableName string) (bool, string) {
	if actual != expected {
		return false, fmt.Sprintf("Values for %s differ: expected=%s but actual=%s", variableName, expected, actual)
	}

	return true, ""
}

func IntEquals(actual int, expected int, variableName string) (bool, string) {
	if actual != expected {
		return false, fmt.Sprintf("Values for %s differ: expected=%d but actual=%d", variableName, expected, actual)
	}

	return true, ""
}

func ByteSliceEquals(actual []byte, expected []byte, variableName string) (bool, string) {
	if !bytes.Equal(actual, expected) {
		return false, fmt.Sprintf("Byte slices for %s differ: expected=%v  but actual=%v", variableName, expected, actual)
	}

	return true, ""
}

func IsNil(something interface{}, variableName string) (bool, string) {
	svalue := fmt.Sprintf("%v", something)
	if something != nil && svalue != "<nil>" {
		return false, fmt.Sprintf("%s should be nil", variableName)
	}

	return true, ""
}

func IsNotNil(something interface{}, variableName string) (bool, string) {
	svalue := fmt.Sprintf("%v", something)
	if something == nil || svalue == "<nil>" {
		return false, fmt.Sprintf("%s should be not nil", variableName)
	}

	return true, ""
}

func NoError(err error) (bool, string) {
	if err != nil {
		return false, fmt.Sprintf("Unexpected err %s", err.Error())
	}

	return true, ""
}

func ErrorMessageContains(err error, substring string) (bool, string) {
	if err == nil {
		return false, fmt.Sprintf("Expecting error message containing '%s', but got nil", substring)
	}

	actualErrorMessage := err.Error()
	if !strings.Contains(actualErrorMessage, substring) {
		return false, fmt.Sprintf("Expecting error message containing '%s', but was '%s'", substring, actualErrorMessage)
	}

	return true, ""
}

func ErrorMessageDoesNotContain(err error, substring string) (bool, string) {
	if err == nil {
		return false, fmt.Sprintf("Expecting error message not containing '%s', but got nil", substring)
	}

	actualErrorMessage := err.Error()
	if strings.Contains(actualErrorMessage, substring) {
		return false, fmt.Sprintf("Expecting error message not containing '%s', but was '%s'", substring, actualErrorMessage)
	}

	return true, ""
}
